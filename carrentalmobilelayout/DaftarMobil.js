import React from "react";
import {ScrollView, Image, StyleSheet, View, Text} from "react-native";
import Feather from "react-native-vector-icons/Feather";
import {content} from "./Content.js"

const DaftarMobil = () => {
        return(
            <ScrollView>
            {content.map((item) => {
                return(
                    <View style={styles.container} key={item.id}>
                    <View style={styles.content}>
                        <Image style={styles.image} source={item.image}/>
                        <View style={styles.frame}>
                        <Text style={styles.textName}>{item.name}</Text>
                            <View style={styles.frame2}>
                                <Feather style={{color: "gray", paddingRight: 12}} name={item.userImg}>4</Feather>
                                <Feather style={{color: "gray"}} name={item.briefImg}>2</Feather>
                            </View>
                            <Text style={styles.textPrice}>{item.price}</Text>
                        </View>
                    </View>
                    </View>
                )
            })}
        </ScrollView>
        )
}

const styles = StyleSheet.create({
    container: {
        display: "flex",
        flexDirection: "column",
        alignItems: "flex-start",
        padding: 16,
        width: 328,
        height: 98,
        borderRadius: 4,
        backgroundColor: "#FFFFFF",
        marginLeft: 16,
        marginTop: 8,
    },
    content:{
        display: "flex",
        flexDirection: "row",
        alignSelf: "flex-start",
        width: 296,
        height: 66,
        marginTop: 10

    },
    frame: {
        display: "flex",
        flexDirection: "column",
        alignItems: "flex-start",
        width: 240,
        height: 66,
        marginBottom: 16
    },
    frame2:{
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-evenly"

    },
    image: {
        marginBottom: 10,
    },
    textName:{
        color: "black",
        fontSize: 14,
        paddingBottom: 6
    },
    textPrice:{
        fontSize: 14,
        color: "#5CB85F"

    }
});

export default DaftarMobil;