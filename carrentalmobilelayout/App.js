import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  useColorScheme,
  View,
  Image
} from 'react-native';
import MainNavigation from './MainNavigation';
import {
  Colors
} from 'react-native/Libraries/NewAppScreen';

const App = () => {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  const [splash, setSplash] = useState(true);
  useEffect (() => {
    setTimeout(() => {
      setSplash(false);
    }, 5000);
  }, []);

  return splash ? (
    <View>
    <Image source={require('./assets/Splash.png')} />  
    </View>) : (
        <MainNavigation />
  );
};

export default App;
 