import React from "react";
import {StyleSheet, View, Text, Image, Button} from "react-native";


const AkunScreen = () => {
        return(
            <View style={styles.container}>
                <Image style={styles.image} source={require('./assets/Allura.png')} />
                <Text style={styles.textContent}>Upss kamu belum memiliki akun. Mulai buat akun agar transaksi di BCR lebih mudah</Text>
                <Button color="#5CB85F" style={styles.button} title="Register"/>
            </View>
        )
}


const styles = StyleSheet.create({
    container: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        width: 312,
        height: 300
    },
    image: {
        width: 312,
        height: 192,
        marginTop: 16,
        marginTop: 16
    },
    textContent: {
        color: "black",
        fontSize: 14,
        marginBottom: 16
    },
    button:{
        paddingTop: 16,
        borderRadius: 2,
        flexDirection: "row",
        justifyContent: "center",
        marginTop: 16
    },
});

export default AkunScreen;