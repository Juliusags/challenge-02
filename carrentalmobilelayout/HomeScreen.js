import React from "react";
import {StyleSheet, View, Text, Image, ImageBackground, Button} from "react-native";
import DaftarMobil from "./DaftarMobil.js";


const HomeScreen = () => {
        return(
            <View style={style.container}>
                <View style={{flex:2}}>
                    <View style={style.headrectangle}>
                        <View style={style.contenthead}>
                            <View>
                                <Text> Hi, Nama </Text>
                                <Text style={{fontWeight: "bold"}}> Your Location</Text>
                            </View>
                            <Image style={style.image} source={require('./assets/profile.png')} />
                        </View>
                        <View style={{paddingTop: 10}}>
                        <ImageBackground source={require("./assets/Bg.png")} style={style.rectangle}>
                            <Text style={style.tagline}>
                                Sewa Mobil Berkualitas {"\n"}
                                di kawasanmu
                            </Text>
                            <View style={style.buttonOuterLayout}>
                            <Button color="#5CB85F" title="Sewa Mobil" style={style.buttonLayout}/>
                            </View>
                        </ImageBackground>
                        </View>
                    </View>
            
                    <View style={style.submenu}>
                        <Image source={require("./assets/icon_truck.png")}/>
                        <Image source={require("./assets/icon_truck.png")}/>
                        <Image source={require("./assets/icon_truck.png")}/>
                        <Image source={require("./assets/icon_truck.png")}/>
                    </View>
                    <View style={{flexDirection: "row", justifyContent: "space-evenly", width: 328, paddingTop: 5}}>
                        <Text style={{fontSize: 12, color:"black"}}>Sewa Mobil</Text>
                        <Text style={{fontSize: 12, color:"black"}}>Oleh-oleh</Text> 
                        <Text style={{fontSize: 12, color:"black"}}>Penginapan</Text>
                        <Text style={{fontSize: 12, color:"black"}}>Wisata</Text>
                    </View>
                </View>
                <View style={{flex:2}}>
                    <Text style={{textAlign: "left", fontSize: 14, color: "black", padding: 10}}>Daftar Mobil Pilihan</Text>
                    <DaftarMobil></DaftarMobil>
                </View>
            </View>
           
        )
}

const style = StyleSheet.create({
    container : {
        flex: 1,
        justifyContent: "center",
        width: 360,
    },
    rectangle : {
        width: 328,
        height: 140,
        alignSelf: "center"
        
    },
    contenthead : {
        flexDirection: "row",
        justifyContent: "space-between",
        paddingLeft: 16,
        paddingRight: 16,
        paddingTop: 16
    },
    headrectangle : {
        width: 360,
        height: 146,
        backgroundColor: "#D3D9FD"
    },
    submenu : {
        flex:1,
        flexDirection: "row",
        justifyContent: "space-evenly",
        alignItems: "center",
        width: 328,
        paddingLeft: 10,
        paddingTop: 65,
        color: "black"
    },
    tagline : {
        fontSize: 16,
        color: "white",
        marginTop: 24,
        marginLeft: 24
    },
    buttonOuterLayout: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingLeft: 20
      },
    buttonLayout: {
        marginBottom: 10
    }


})

export default HomeScreen;