import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/Feather';

import HomeScreen from './HomeScreen';
import DaftarMobil from './DaftarMobil';
import AkunScreen from './AkunScreen';

const home = "Home";
const daftarMobil = "Daftar Mobil";
const akun = "Akun";

const Tab = createBottomTabNavigator();

function MainNavigation () {
    return (
        <NavigationContainer>
            <Tab.Navigator
                initialRouteName={home}
                screenOptions={{
                    activeTintColor: '#0D28A6',
                    inactiveTintColor: '#222222',
                    labelStyle: {
                        fontSize: 12,
                        fontWeight: 'bold'
                    },
                    style: {
                        backgroundColor: '#fff',
                        borderTopWidth: 0.5,
                        borderTopColor: '#000'
                    }
                }}
            >
                <Tab.Screen
                    name={home}
                    component={HomeScreen}
                    options={{
                        tabBarLabel: 'Home',
                        headerShown: false,
                        tabBarIcon: ({ color, size }) => (
                            <Icon name="home" color={color} size={size} />
                        )
                    }}
                />
                <Tab.Screen
                    name={daftarMobil}
                    component={DaftarMobil}
                    options={{
                        tabBarLabel: 'Daftar Mobil',
                        tabBarIcon: ({ color, size }) => (
                            <Icon name="list" color={color} size={size} />
                        )
                    }}
                />
                <Tab.Screen
                    name={akun}
                    component={AkunScreen}
                    options={{
                        tabBarLabel: 'Akun',
                        tabBarIcon: ({ color, size }) => (
                            <Icon name="user" color={color} size={size} />
                        )
                    }}
                />
                    
            </Tab.Navigator>
        </NavigationContainer>
    )
}

export default MainNavigation;